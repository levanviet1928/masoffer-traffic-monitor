@rem
@rem Copyright 2015 the original author or authors.
@rem
@rem Licensed under the Apache License, Version 2.0 (the "License");
@rem you may not use this file except in compliance with the License.
@rem You may obtain a copy of the License at
@rem
@rem      https://www.apache.org/licenses/LICENSE-2.0
@rem
@rem Unless required by applicable law or agreed to in writing, software
@rem distributed under the License is distributed on an "AS IS" BASIS,
@rem WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@rem See the License for the specific language governing permissions and
@rem limitations under the License.
@rem

@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  masoffer-traffic-monitor startup script for Windows
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%..

@rem Resolve any "." and ".." in APP_HOME to make it shorter.
for %%i in ("%APP_HOME%") do set APP_HOME=%%~fi

@rem Add default JVM options here. You can also use JAVA_OPTS and MASOFFER_TRAFFIC_MONITOR_OPTS to pass JVM options to this script.
set DEFAULT_JVM_OPTS="-Duser.dir=%~dp0.." "-Dvertx.cwd=%~dp0.." "-Dlogback.configurationFile=%~dp0../conf/logback.groovy" "-Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.SLF4JLogDelegateFactory" "-Xmx512m"

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if "%ERRORLEVEL%" == "0" goto execute

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto execute

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:execute
@rem Setup the command line

set CLASSPATH=%APP_HOME%\lib\masoffer-traffic-monitor-21.06.26.0-SNAPSHOT.jar;%APP_HOME%\lib\groovy-all-2.4.10.jar;%APP_HOME%\lib\gpars-1.2.1.jar;%APP_HOME%\lib\logback-classic-1.2.1.jar;%APP_HOME%\lib\logback-core-1.2.1.jar;%APP_HOME%\lib\commons-lang3-3.5.jar;%APP_HOME%\lib\vertx-web-3.4.1.jar;%APP_HOME%\lib\vertx-auth-shiro-3.4.1.jar;%APP_HOME%\lib\vertx-auth-common-3.4.1.jar;%APP_HOME%\lib\mongodb-driver-async-3.4.3.jar;%APP_HOME%\lib\jackson-dataformat-cbor-2.8.7.jar;%APP_HOME%\lib\jackson-dataformat-smile-2.8.7.jar;%APP_HOME%\lib\redisson-3.13.6.jar;%APP_HOME%\lib\vertx-core-3.4.1.jar;%APP_HOME%\lib\jackson-dataformat-yaml-2.11.1.jar;%APP_HOME%\lib\jackson-databind-2.11.1.jar;%APP_HOME%\lib\jackson-core-2.11.1.jar;%APP_HOME%\lib\jackson-annotations-2.11.1.jar;%APP_HOME%\lib\failsafe-1.0.4.jar;%APP_HOME%\lib\completable-futures-0.3.0.jar;%APP_HOME%\lib\fastjson-1.2.7.jar;%APP_HOME%\lib\multiverse-core-0.7.0.jar;%APP_HOME%\lib\jsr166y-1.7.0.jar;%APP_HOME%\lib\slf4j-api-1.7.30.jar;%APP_HOME%\lib\shiro-core-1.2.4.jar;%APP_HOME%\lib\mongodb-driver-core-3.4.3.jar;%APP_HOME%\lib\bson-3.4.3.jar;%APP_HOME%\lib\snakeyaml-1.27.jar;%APP_HOME%\lib\netty-resolver-dns-4.1.52.Final.jar;%APP_HOME%\lib\netty-codec-http2-4.1.8.Final.jar;%APP_HOME%\lib\netty-handler-4.1.52.Final.jar;%APP_HOME%\lib\netty-handler-proxy-4.1.8.Final.jar;%APP_HOME%\lib\netty-codec-http-4.1.8.Final.jar;%APP_HOME%\lib\netty-codec-dns-4.1.52.Final.jar;%APP_HOME%\lib\netty-codec-socks-4.1.8.Final.jar;%APP_HOME%\lib\netty-codec-4.1.52.Final.jar;%APP_HOME%\lib\netty-transport-4.1.52.Final.jar;%APP_HOME%\lib\netty-buffer-4.1.52.Final.jar;%APP_HOME%\lib\netty-resolver-4.1.52.Final.jar;%APP_HOME%\lib\netty-common-4.1.52.Final.jar;%APP_HOME%\lib\cache-api-1.0.0.jar;%APP_HOME%\lib\reactor-core-3.3.9.RELEASE.jar;%APP_HOME%\lib\rxjava-2.2.19.jar;%APP_HOME%\lib\jboss-marshalling-river-2.0.10.Final.jar;%APP_HOME%\lib\byte-buddy-1.10.14.jar;%APP_HOME%\lib\jodd-bean-5.1.6.jar;%APP_HOME%\lib\commons-beanutils-1.8.3.jar;%APP_HOME%\lib\reactive-streams-1.0.3.jar;%APP_HOME%\lib\jboss-marshalling-2.0.10.Final.jar;%APP_HOME%\lib\jodd-core-5.1.6.jar


@rem Execute masoffer-traffic-monitor
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %MASOFFER_TRAFFIC_MONITOR_OPTS%  -classpath "%CLASSPATH%" app.Runner %*

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable MASOFFER_TRAFFIC_MONITOR_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%MASOFFER_TRAFFIC_MONITOR_EXIT_CONSOLE%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
