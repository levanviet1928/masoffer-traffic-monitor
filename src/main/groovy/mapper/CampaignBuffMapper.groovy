package mapper

import model.CampaignBuff

class CampaignBuffMapper extends ModelMapper<CampaignBuff>{

    CampaignBuffMapper() {
        super(CampaignBuff.class)
    }
}
