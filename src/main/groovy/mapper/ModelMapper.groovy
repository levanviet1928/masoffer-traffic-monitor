package mapper

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.ser.std.DateSerializer
import groovy.transform.CompileStatic
import org.apache.commons.lang3.reflect.TypeUtils
import org.bson.Document
import org.bson.types.ObjectId

/**
 * Created by dangnh@eway.vn on 10/20/2016.
 */
@CompileStatic
class ModelMapper<M> {

    final Class<M> resourceClass

    static ObjectMapper mapper = new ObjectMapper()
    static ObjectMapper documentMapper = new ObjectMapper()

    static {
        // Non-standard JSON but we allow C style comments in our JSON
        mapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true)
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)

        documentMapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true)
        documentMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)
        documentMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
        documentMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
        documentMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        documentMapper.registerModule(new SimpleModule() {
            {
                addSerializer(Date.class, new DateSerializer() {
                    @Override
                    void serialize(Date value, JsonGenerator gen, SerializerProvider provider) throws IOException {
                        gen.writeRawValue('{ "$date" : ' + value.time + ' }')
                    }
                })

            }
        })
    }

    ModelMapper(Class<M> resourceClass) {
        this.resourceClass = resourceClass
    }

    List<Document> toDocuments(List<M> models) {
        if (models?.isEmpty()) {
            return null
        }
        return models.collect { model -> this.toDocument(model) }
    }

    List<M> toModels(List<Document> datas) {
        if (datas?.isEmpty()) {
            return []
        }
        return datas.collect { data -> this.toModel(data) } as List<M>
    }

    M toModel(Document document) {
        if (document == null) return null
        try {
            if (document.get("_id") != null) {
                document.put("_id", Objects.toString(document.get("_id"), null))
            }
            String documentJson = mapper.writeValueAsString(document)
            return mapper.readValue(documentJson, resourceClass)
        } catch (Exception e) {
            throw new RuntimeException("Failed to encode as JSON", e)
        }
    }

    Document toDocument(M model) {
        if (model == null) return null
        try {
            String documentJson = documentMapper.writeValueAsString(model)
            Document document = Document.parse(documentJson)
            if (document.get("_id") != null && TypeUtils.isInstance(document.get("_id"), String.class)) {
                document.put("_id", new ObjectId(document.getString("_id")))
            }
            return document
        } catch (Exception e) {
            throw new RuntimeException("Failed to encode as JSON", e)
        }
    }
}
