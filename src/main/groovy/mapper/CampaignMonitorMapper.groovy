package mapper

import model.CampaignMonitor

class CampaignMonitorMapper extends ModelMapper<CampaignMonitor> {

  CampaignMonitorMapper() {
    super(CampaignMonitor.class)
  }
}
