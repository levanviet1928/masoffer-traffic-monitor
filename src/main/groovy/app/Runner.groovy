package app

import groovy.util.logging.Slf4j
import org.redisson.api.RedissonClient
import worker.CampaignMonitorWorker

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

@Slf4j
class Runner {

    static void main(String[] args) {
        def appConfigFile = new File(System.getProperty('user.dir'), 'conf/application.yml')
        def config = AppConfig.newInstance(appConfigFile)
        log.debug("Application start!")
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(1)
        ExecutorService redisFixedThreadPool = Executors.newFixedThreadPool(1)
        scheduledThreadPool.scheduleWithFixedDelay({
            String campaignId = config.campaignId
            RedissonClient redissonClient = config.redissonClient
            CampaignMonitorWorker monitorWorker = new CampaignMonitorWorker(
                redissonClient: redissonClient,
                campaignMonitorCollection: config.campaignMonitorCollection,
                campaignBuffCollection: config.campaignBuffCollection,
                campaignId: campaignId,
            )
            redisFixedThreadPool.submit(monitorWorker)
        }, 0, 5, TimeUnit.MINUTES)
    }
}
