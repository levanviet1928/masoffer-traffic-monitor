package worker

import groovy.util.logging.Slf4j
import io.mongodb.collection.CampaignBuffCollection
import io.mongodb.collection.CampaignMonitorCollection
import model.CampaignMonitor
import org.redisson.api.RedissonClient

@Slf4j
class CampaignMonitorWorker implements Runnable {

    RedissonClient redissonClient
    CampaignMonitorCollection campaignMonitorCollection
    CampaignBuffCollection campaignBuffCollection
    String campaignId

    @Override
    void run() {
        CampaignMonitor campaignByDate = campaignMonitorCollection.findByCampaignId(campaignId)
        Long singlePageUsers = redissonClient.getAtomicLong("${campaignId}-load").getAndSet(0)
        Long normalUsers = redissonClient.getAtomicLong("${campaignId}-all-actions").getAndSet(0)
        def campaignMonitor
        if (campaignByDate == null) {
            CampaignMonitor campaign = new CampaignMonitor(campaignId: campaignId, singlePageUsers: singlePageUsers, normalUsers: normalUsers)
            campaignMonitorCollection.insertTrafficMonitor(campaign)
            return
        }
        campaignMonitorCollection.updateTrafficMonitor(campaignId, singlePageUsers, normalUsers)
    }
}
