package model

import com.fasterxml.jackson.annotation.JsonProperty

import java.sql.Date

class CampaignBuff {

    @JsonProperty("_id")
    String campaignId

    @JsonProperty("start_date")
    Date startDate

    @JsonProperty("end_date")
    Date endDate

    @JsonProperty("target_users")
    Integer targetUsers

    @JsonProperty("daily_users")
    Integer dailyUsers

    @JsonProperty("bounce_rate")
    Float bounceRate

    @JsonProperty("daily_single_page_users")
    Integer dailySinglePage

    @JsonProperty("daily_normal_users")
    Integer dailyNormalUsers

    @JsonProperty("estimate_session_duration")
    Integer estimateSessionDuration

    @JsonProperty("base_urls")
    List<String> baseUrls

    @JsonProperty("actions")
    List<Action> actions

}
