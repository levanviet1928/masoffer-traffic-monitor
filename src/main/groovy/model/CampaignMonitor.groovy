package model

import com.fasterxml.jackson.annotation.JsonProperty

class CampaignMonitor {

    @JsonProperty("campaign_id")
    String campaignId

    @JsonProperty("daily_single_page_users")
    Long singlePageUsers

    @JsonProperty("daily_normal_users")
    Long normalUsers

}
