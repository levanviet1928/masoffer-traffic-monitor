package io.mongodb

import com.mongodb.ConnectionString
import com.mongodb.async.client.MongoClient
import com.mongodb.async.client.MongoClients
import com.mongodb.async.client.MongoDatabase
import groovy.transform.CompileStatic
import org.apache.commons.lang3.Validate

/**
 * For MongoDB Async Driver
 */
@CompileStatic
class DocumentCollection {

    @Delegate
    protected final SuperMongoCollection delegate

    private MongoDatabase database
    /**
     * URI Example: io.mongodb://username:password@host:port/database.collection
     * @param uri
     */
    DocumentCollection(String uri) {
        Validate.notBlank(uri, "uri must be not blank")

        ConnectionString connectionString = new ConnectionString(uri)
        Validate.notBlank(connectionString.database, "database name must be not blank in URI")
        Validate.notBlank(connectionString.collection, "collection name must be not blank in URI")

        MongoClient mongoClient = MongoClients.create(uri)
        this.database = mongoClient.getDatabase(connectionString.database)
        this.delegate = database.getCollection(connectionString.collection).withTraits(SuperMongoCollection.class)
    }

    SuperMongoCollection getAnotherCollection(String name){
        return database.getCollection(name) as SuperMongoCollection
    }
}

