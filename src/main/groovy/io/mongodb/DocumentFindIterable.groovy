package io.mongodb

import com.mongodb.Block
import com.mongodb.CursorType
import com.mongodb.async.AsyncBatchCursor
import com.mongodb.async.SingleResultCallback
import com.mongodb.async.client.FindIterable
import com.mongodb.client.model.Collation
import org.bson.Document
import org.bson.conversions.Bson

import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

/**
 * Created by chipn@eway.vn on 2/9/17.
 */
class DocumentFindIterable {

    final FindIterable<Document> delegate

    DocumentFindIterable(FindIterable delegate) {
        this.delegate = delegate
    }

    DocumentFindIterable filter(Bson filter) {
        delegate.filter(filter)
        return this
    }

    DocumentFindIterable limit(int limit) {
        delegate.limit(limit)
        return this
    }

    DocumentFindIterable skip(int skip) {
        delegate.skip(skip)
        return this
    }

    DocumentFindIterable maxTime(long maxTime, TimeUnit timeUnit) {
        delegate.maxTime(maxTime, timeUnit)
        return this
    }

    DocumentFindIterable maxAwaitTime(long maxAwaitTime, TimeUnit timeUnit) {
        delegate.maxAwaitTime(maxAwaitTime, timeUnit)
        return this
    }

    DocumentFindIterable modifiers(Bson modifiers) {
        delegate.modifiers(modifiers)
        return this
    }

    DocumentFindIterable projection(Bson projection) {
        delegate.projection(projection)
        return this
    }

    DocumentFindIterable sort(Bson sort) {
        delegate.sort(sort)
        return this
    }

    DocumentFindIterable noCursorTimeout(boolean noCursorTimeout) {
        delegate.noCursorTimeout(noCursorTimeout)
        return this
    }

    DocumentFindIterable oplogReplay(boolean oplogReplay) {
        delegate.oplogReplay(oplogReplay)
        return this
    }

    DocumentFindIterable partial(boolean partial) {
        delegate.partial(partial)
        return this
    }

    DocumentFindIterable cursorType(CursorType cursorType) {
        delegate.cursorType(cursorType)
        return this
    }

    CompletableFuture<Document> first() {
        MongoCompletableFuture<Document> future = new MongoCompletableFuture<>()
        delegate.first(future)
        return future
    }

    CompletableFuture<List<Document>> intoDocuments() {
        MongoCompletableFuture<List<Document>> future = new MongoCompletableFuture<>()
        delegate.into(new ArrayList<Document>(), future)
        return future
    }

    void forEach(Block<Document> block, SingleResultCallback<Void> callback) {
        delegate.forEach(block, callback)
    }

    DocumentFindIterable batchSize(int batchSize) {
        delegate.batchSize(batchSize)
        return this
    }

    CompletableFuture<DocumentBatchCursor> batchCursor() {
        MongoCompletableFuture<AsyncBatchCursor<Document>> future = new MongoCompletableFuture<>()
        delegate.batchCursor(future)
        return future.thenApply { asyncBatchCursor -> new DocumentBatchCursor(asyncBatchCursor) }
    }

    DocumentFindIterable collation(Collation collation) {
        delegate.collation(collation)
        return this
    }
}