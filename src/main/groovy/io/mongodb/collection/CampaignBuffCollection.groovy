package io.mongodb.collection

import io.mongodb.ModelCollection
import mapper.CampaignBuffMapper
import model.CampaignBuff
import org.bson.Document

class CampaignBuffCollection extends ModelCollection<CampaignBuff> {

  CampaignBuffCollection(String uri) {
    super(uri, CampaignBuffMapper.class)
  }

  CampaignBuff findCampaignById(String campaignId) {
    Document filter = [
      "campaign_id": campaignId
    ]
    this.findModels(filter).first().join()
  }

}
