package io.mongodb.collection

import io.mongodb.ModelCollection
import mapper.CampaignMonitorMapper
import model.CampaignMonitor
import org.bson.Document
import util.Constants

class CampaignMonitorCollection extends ModelCollection<CampaignMonitor> {
    String currentDate = new Date(System.currentTimeMillis()).format(Constants.DATE_PATTERN)

    CampaignMonitorCollection(String uri) {
        super(uri, CampaignMonitorMapper.class)
    }

    CampaignMonitor findByCampaignId(String campaignId) {
        campaignId = "${campaignId}-${currentDate}"
        Document filter = [
            campaign_id: campaignId,
        ]
        this.findModels(filter).first().join()
    }

    CampaignMonitor updateTrafficMonitor(String campaignId, Long singleUsers, Long normalUsers) {
        campaignId = "${campaignId}-${currentDate}"
        Document filter = [
            campaign_id: campaignId,
        ]

        Document update = [
            '$inc': [
                daily_single_page_users: singleUsers,
                daily_normal_users     : normalUsers
            ],
        ]
        return this.findOneAndUpdateModel(filter, update).join()
    }

    CampaignMonitor insertTrafficMonitor(CampaignMonitor trafficMonitor) {
        trafficMonitor.setCampaignId("${trafficMonitor.campaignId}-${currentDate}")
        return this.insertOneModel(trafficMonitor).join()
    }
}
